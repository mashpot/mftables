/*
 * filter.c
 *
 * Created 20 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <regex.h>

#include <addrbook.h>

static size_t parsepats (char *buf, size_t len)
{
    size_t count = 0;
    char *p, *e;

    p = buf;
    e = p + len;

    for (; p < e; ++count)
    {
        while (p < e && *p == '\n')
            ++p;
        if (p >= e)
            break;

        do
            *buf++ = *p++;
        while (p < e && *p != '\n');

        *buf++ = 0;
    }

    return count;
}

static ssize_t loadfilter (const char *pathname, regex_t **pregv)
{
    int fd;
    off_t off, len;
    ssize_t n;
    size_t ln;
    regex_t *regv;

    fd = open (pathname, O_RDONLY);

    if (fd == -1) {
        perror ("loadfilter:open()");
        return -1;
    }

    len = lseek (fd, 0, SEEK_END);

    if (len == -1) {
        perror ("loadfilter:lseek()");
        close (fd);
        return -1;
    }

    char buf[len+1];

    for (off = 0; off < len; off += n)
    {
        n = pread (fd, &buf[off], len - off, off);
        if (n <= 0) {
            if (n == 0)
                fprintf (stderr, "loadfilter: EOF while reading filter '%s'\n", pathname);
            else
                perror ("loadfilter:pread()");
            close (fd);
            return -1;
        }
    }

    ln = parsepats (&buf[0], len);

    regv = malloc (sizeof (regex_t) * ln);

    if (regv == NULL) {
        perror ("loadfilter:malloc()");
        close (fd);
    }
    *pregv = regv;
    memset (regv, 0, sizeof (regex_t) * ln);

    off = 0;
    for (n = 0; n < ln; ++n)
    {
        //printf ("%s\n", &buf[off]);
        int err = regcomp (&regv[n], &buf[off], REG_NOSUB);

        if (err) {
            fprintf (stderr, "mftables: Invalid pattern '%s'\n", &buf[off]);
            for (ln = 0; ln < n; ++ln)
                regfree (&regv[n]);
            close (fd);
            return -1;
        }
        off += strlen (&buf[off]) + 1;
    }

    return ln;
}

static void regvfree (regex_t *regv, size_t regc)
{
    regex_t *reg, *end;
    reg = regv;
    end = regv + regc;
    while (reg < end)
        regfree (reg++);
    free (regv);
}

static int nregmatch (regex_t *regv, size_t regc, const char *buf)
{
    regex_t *end = regv + regc;
    int n = 0;
    while (regv < end)
    {
        if (regexec (regv++, buf, 0, NULL, 0) == 0)
            ++n;
    }
    return n;
}

static int isregmatch (regex_t *regv, size_t regc, const char *buf)
{
    regex_t *end = regv + regc;
    while (regv < end)
    {
        if (regexec (regv++, buf, 0, NULL, 0) == 0)
            return 1;
    }
    return 0;
}

int filter_addrbook (const char *fpath, struct addrbook *ab)
{
    regex_t *regv;
    ssize_t n;

    if ((n = loadfilter (fpath, &regv)) == -1)
        return -1;

    char buf[AB_MAXEMAIL + 1];
    size_t len;
    struct user *u, *e;
    struct domain *d;
    u32 dimax;

    d = ab->dmem;
    dimax = ab->ndomain;

    u = ab->umem;
    e = u + ab->nuser;

    for (; u < e; ++u)
    {
        if (u->dom_id >= dimax) {
            printf ("[ERROR] Corrupt domain index: %s\n", &u->name[0]);
            break;
        }

        len = strlen (&u->name[0]);
        strcpy (&buf[0], &u->name[0]);
        buf[len] = '@';
        strcpy (&buf[len+1], &d[u->dom_id].name[0]);

        u->score = 0;

        if (isregmatch (regv, n, &buf[0])) {
            u->score = 255;
            break;
        }
    }

    regvfree (regv, n);
    ab->doff = ab->uoff = 0;

    return 0;
}

#if 1
int filter_scoreaddrs (const char *fpath, int adrc, const char *adrv[])
{
    regex_t *regv;
    ssize_t i, n;
    int nm, score = 0;

    if ((n = loadfilter (fpath, &regv)) == -1)
        return -1;

    for (i = 0; i < adrc; ++i)
    {
        nm = nregmatch (regv, n, adrv[i]);
        score += nm < 2 ? nm : 2;
    }

    float fsc = (score / (float) adrc) / 2.0f;
    score = fminf (roundf (fsc*256.0f), 255.0f);

    regvfree (regv, n);

    return score;
}
#endif

