/*
 * parse.c
 *
 * Created 31 May 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <addrbook.h>
#include <mbox.h>
#include <parse.h>


#define MAXBUF 65536

static int readmbox (int fd, struct mbox *mb)
{
    char buf[MAXBUF];
    size_t len;
    ssize_t n;
    int state;

    memset (mb, 0, sizeof (struct mbox));
    len = 0;
    state = 0;

    for (; ;)
    {
        n = read (fd, &buf[len], MAXBUF-1 - len);
        if (n == -1) {
            perror ("read_mbox:read()");
            return -1;
        }
        len += n;

        state = mbox_parse (&buf[0], len, state, mb);
        if (state == -1) {
            fprintf (stderr, "read_mbox:mbox_parse(): Failed\n");
            return -1;
        }
        if (state == 4) {
            break;
        }
    }

    return 0;
}

static char *parseaddr (const char *str, size_t len)
{
    char buf[len+1], *p, *s;
    p = &buf[0];

    memcpy (p, str, len);
    p[len] = 0;

    s = strchr (p, '@');
    if (s == NULL)
        return NULL;

    while (s > p && *s != ' ' && *s != '\t' && *s != '<')
        --s;

    p = *s++ == '<' ? strchr (s, '>') : strchr (s, ' ');

    if (p == NULL)
        p = &buf[len];

    if (p != s && (p[-1] == ',' || p[-1] == '\n'))
        --p;

    *p = 0;
    len = p - s;

    if (len == 0)
        return NULL;

    char *addr = malloc (len + 1);

    if (addr == NULL) {
        perror ("parseaddr:malloc()");
        return NULL;
    }
    strcpy (addr, s);

    return addr;
}

static int getrecipients (struct mbox *mb, char ***padrv)
{
    struct mbheader **hdv, **end, *hd;
    const char *s, *p;
    char *addr;
    size_t len, adrvlen, adrc;
    char **adrv;

    adrc = 0;
    adrvlen = 8;
    adrv = malloc (sizeof (char*) * adrvlen);

    if (adrv == NULL) {
        perror ("getrecipients:malloc()");
        return -1;
    }

    hdv = mb->headv;
    end = hdv + mb->headc;

    while (hdv < end)
    {
        hd = *hdv++;

        if (!hd->key || !hd->val)
            continue;
        switch (hd->key[0])
        {
          case 'T':
            if (hd->key[1] != 'o' || hd->key[2] != 0)
                continue;
            break;
          case 'C':
            if (hd->key[1] != 'c' || hd->key[2] != 0)
                continue;
            break;
          default:
            continue;
        }

        s = hd->val;
        p = s-1;
        do {
            s = p + 1;
            p = strchr (s, '\n');

            len = p ? p - s : strlen (s);

            addr = parseaddr (s, len);

            if (addr == NULL) {
                fprintf (stderr, "getrecipients: Invalid email address '%.*s'\n", (int)len, s);
                continue;
            }

            if (adrc >= adrvlen) {
                adrvlen += 8;
                adrv = realloc (adrv, sizeof (char*) * adrvlen);
                if (adrv == NULL) {
                    perror ("getrecipients:realloc()");
                    return -1;
                }
            }
            adrv[adrc++] = addr;

        } while (p);
    }

    if (adrc == 0) {
        free (adrv);
    } else if (adrc < adrvlen) {
        adrv = realloc (adrv, sizeof (char*) * adrc);
        if (adrv == NULL) {
            perror ("getrecipients:realloc()");
            return -1;
        }
    }

    *padrv = adrv;

    return adrc;
}


int parse_mail (int fd, struct addrbook *ab)
{
    struct mbox mb;
    char **adrv;
    int adrc;

    if (readmbox (fd, &mb) == -1)
        return -1;

    if (abook_add (ab, mb.from) == -1)
        goto err;

    adrc = getrecipients (&mb, &adrv);
    if (adrc == -1)
        goto err;

    for (int i = 0; i < adrc; ++i)
    {
        printf ("%s\n", adrv[i]);
    }

err:
    mbox_parse (0, 0, 4, &mb);
    return -1;
}

struct email_info parse_email(int fd)
{
    struct email_info ei;
    char buf[MAXBUF];
    char *p, *e, *s, *v;
    size_t len;
    ssize_t n;

    memset (&ei, 0, sizeof (ei));

    len = 0;
    for (n = 1, e = NULL, p = &buf[0];;)
    {
        if (e == NULL) {
            if (n == 0)
                break;
            //printf ("len= %lu\n", len);
            n = read (fd, p + len, (MAXBUF) - len);
            if (n == -1) {
                perror ("parse_email:read()");
                goto err;
            }
            e = p + len + n;
            //e = p + n;
            *e = 0;
        }

        s = strchr (p, '\n');

        if (s == NULL) {
            len = e - p;
            if (len >= MAXBUF-1) {
                fprintf (stderr, "parse_email: Header exceeds MAXBUF\n");
                break;
            }
            memmove (&buf[0], p, len);
            p = &buf[0];
            e = NULL;
            continue;
        }
        *s = 0;

        if (s - p > 4 && memcmp (p, "----", 4) == 0)
            break;

        v = strchr (p, ':');

        if (v == NULL) {
            p = s + 1;
            continue;
        }

        len = v - p;

        switch (len)
        {
          default:
            p = s + 1;
            continue;
          case 2:
            if (ei.to || (p[0] | 32) != 't' || p[1] != 'o')
                break;
            v++;
            p = strchr (v, '<');

            if (p == NULL)
                break;
            p++;
            v = strchr (p, '>');

            if (v == NULL)
                break;
            *v = 0;
            len = v - p;
            ei.to = malloc (len + 1);

            if (ei.to == NULL) {
                perror ("parse_email:malloc()");
                goto err;
            }
            strcpy (ei.to, p);

            break;
          case 4:
            if (ei.from || (p[0] | 32) != 'f' || memcmp (p+1, "rom", 3) != 0)
                break;
            v++;
            p = strchr (v, '<');

            if (p == NULL) {
                p = strrchr (v, ' ');
                if (p == NULL)
                    break;
                p++;
                v = s;
            } else {
                p++;
                v = strchr (p, '>');
            }

            if (v == NULL)
                break;
            *v = 0;
            len = v - p;
            ei.from = malloc (len + 1);

            if (ei.from == NULL) {
                perror ("parse_email:malloc()");
                goto err;
            }
            strcpy (ei.from, p);

            break;
          case 7:
            if (ei.subject || (p[0] | 32) != 's' || memcmp (p+1, "ubject", 6) != 0)
                break;
            if (*++v)
                ++v;
            len = s - v;
            ei.subject = malloc (len + 1);

            if (ei.subject == NULL) {
                perror ("parse_email:malloc()");
                goto err;
            }
            strcpy (ei.subject, v);

            break;

        }

        if (ei.to && ei.from && ei.subject)
            break;
        p = s + 1;
    }

    return ei;

err:
    if (ei.to)
        free (ei.to);
    if (ei.from)
        free (ei.from);
    if (ei.subject)
        free (ei.subject);
    ei.flags = -1;
    return ei;
}

