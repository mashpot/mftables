/*
 * main.c
 *
 * Created 30 May 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <iotools.h>
#include <addrbook.h>
#include <parse.h>
#include <filter.h>

#include <localdata.h>

static const char addrbook_fname[] = "addrbook.db";
static const char errout_fname[] = "stderr.log";
static const char outout_fname[] = "stdout.log";
static const char addrfilter_fname[] = "address.filter";

static int filter (struct addrbook *ab, const char *filter_path)
{
    struct email_info ei;
    int score;

    ei = parse_email (0);

    if (ei.flags == -1)
        return -1;

    if (!ei.from) {
        fprintf (stderr, "Failed to parse from address\n");
        return -1;
    }

    score = abook_add (ab, ei.from);

    if (filter_path) {
        if (filter_addrbook (filter_path, ab) != 0)
            return -1;
        score = abook_add (ab, ei.from);
    }

    return score;
}

#define OPT_EMAIL 1
#define OPT_PRINT 2
#define OPT_FILTER 4
#define OPT_DEBUG 8

int main(int argc, char *argv[])
{
    char *arg, *filter_path;
    int i, opts;

    filter_path = NULL;
    opts = 0;

    for (i = 1; i < argc; ++i)
    {
        arg = argv[i];

        while (*arg == '-')
            ++arg;

        switch (*arg++)
        {
          case 'e':
            if (*arg && strcmp (arg, "mail"))
                break;
            opts |= OPT_EMAIL;
            continue;

          case 'f':
            if (*arg && *arg != '=' &&
                (*arg++ != 'i' ||
                 *arg++ != 'l' ||
                 *arg++ != 't' ||
                 *arg++ != 'e' ||
                 *arg++ != 'r'))
            {
                 break;
            }

            if (*arg == '=') {
                filter_path = ++arg;
            } else if (++i < argc) {
                filter_path = argv[i];
            } else {
                fprintf (stderr, "mftables: Missing filter filepath\n");
                return 1;
            }
            opts |= OPT_FILTER;
            continue;

          case 'p':
            if (*arg && strcmp (arg, "rint"))
                break;
            opts |= OPT_PRINT;
            continue;

          case 'm':
            opts |= OPT_DEBUG;
            continue;

          default:
            break;
        }

        fprintf (stderr, "mftables: Unknown option '%s'\n", argv[i]);
        return 1;
    }

    /*
     * Initalize and construct user local data directory paths
     */
    char *abook_path, *outout_path, *errout_path;

    if (localdata_init () != 0)
        return 1;

    if (!(abook_path = localdata_path (&addrbook_fname[0])) ||
        !(outout_path = localdata_path (&outout_fname[0])) ||
        !(errout_path = localdata_path (&errout_fname[0])))
    {
        fprintf (stderr, "mftables: Error accessing data\n");
        return 1;
    }

    if (opts == 0) {
        opts |= OPT_EMAIL;
#if 1
        if (stdout_redirect (outout_path) != 0 ||
            stderr_redirect (errout_path) != 0)
            return 1;
#endif
    }

    if (!filter_path && !((opts & OPT_PRINT)))
    {
        filter_path = localdata_path (&addrfilter_fname[0]);

        if (!filter_path) {
            fprintf (stderr, "mftables: Error accessing data\n");
            return 1;
        }

        struct stat st;
        memset (&st, 0, sizeof (st));

        if (stat (filter_path, &st) == 0 && S_ISREG (st.st_mode)) {
            opts |= OPT_FILTER;
        } else {
            opts &= ~OPT_FILTER;
            free (filter_path);
            filter_path = NULL;
        }
    }

    /*
     * Perform actions using address book
     */

    struct addrbook ab = abook_open (abook_path);
    int rc = 0;

    if (ab.fd == -1)
        return 1;

    if ((opts & OPT_DEBUG)) {
        parse_mail (0, &ab);
        goto err;
    }

    if ((opts & OPT_EMAIL)) {
        rc = filter (&ab, filter_path);
        if (rc == -1)
            goto err;
    }
    else if ((opts & OPT_FILTER)) {
        if (filter_addrbook (filter_path, &ab) != 0) {
                rc = 1;
            goto err;
        }
    }

    if ((opts & OPT_PRINT)) {
        abook_print (&ab);
    }

err:
    if (abook_close (&ab) == -1)
        return 1;

    if (rc < 0 || rc > 255)
        rc = 1;
    return rc;
}

