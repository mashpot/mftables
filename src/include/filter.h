/*
 * filter.h
 *
 * Created 20 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef FILTER_H_
#define FILTER_H_

int filter_addrbook (const char *fpath, struct addrbook *ab);

#endif /* FILTER_H_ */

