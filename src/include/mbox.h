/*
 * mbox.h
 *
 * Created 21 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef MBOX_H_
#define MBOX_H_

struct mbheader {
    const char *key;
    const char *val;
};

struct mbox {
    size_t pos;
    char *from;
    time_t dt;
    size_t headc;
    struct mbheader **headv;
};

int mbox_parse (char *buf, size_t len, int state, struct mbox *mb);

int mbox_print (const struct mbox *mb);

#endif /* MBOX_H_ */

