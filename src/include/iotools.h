/*
 * ftools.h
 *
 * Created 20 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef FTOOLS_H_
#define FTOOLS_H_

int stdout_redirect (const char *pathname);
int stderr_redirect (const char *pathname);

#endif /* FTOOLS_H_ */

