/*
 * parse.h
 *
 * Created 31 May 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef PARSE_H_
#define PARSE_H_

struct email_info {
    int flags;
    char *to;
    char *from;
    char *subject;
};

struct email_info parse_email(int fd);

int read_mbox (int fd);

int parse_mail (int fd, struct addrbook *ab);

#endif /* PARSE_H_ */

