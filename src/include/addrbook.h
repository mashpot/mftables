/*
 * addrbook.h
 *
 * Created 30 May 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef ADDRBOOK_H_
#define ADDRBOOK_H_

#define AB_MAXDOMAIN 251
#define AB_MAXUSER 58
#define AB_MAXEMAIL (AB_MAXUSER + AB_MAXDOMAIN)

struct domain {
    u32 ucount;
    u8 flags;
    u8 score;
    char name[252];
};

struct user {
    u32 dom_id;
    u8 score;
    char name[59];
};

struct addrbook {
    u32 ndomain;
    u32 nuser;
    u64 dlen;
    u64 ulen;
    struct domain *dmem;
    struct user *umem;
    off_t doff;
    off_t uoff;
    off_t flen;
    int fd;
};

struct addrbook abook_open(const char *pathname);

int abook_close(struct addrbook *ab);

int abook_add(struct addrbook *restrict ab, const char *restrict emailaddr);

void abook_print(const struct addrbook *ab);

#endif /* ADDRBOOK_H_ */

