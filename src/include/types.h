/*
 * types.h
 *
 * Created 21 Nov 2022 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C)  Mason Hall
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <arch.h>

#define I8_MAX  (127)
#define I16_MAX (32767)
#define I32_MAX (2147483647L)
#define I64_MAX (9223372036854775807ULL)

#define U8_MAX  (0xffU)
#define U16_MAX (0xffffU)
#define U32_MAX (0xffffffffUL)
#define U64_MAX (0xffffffffffffffffULL)

typedef char i8;
typedef unsigned char u8;

typedef short i16;
typedef unsigned short u16;

typedef int i32;
typedef unsigned int u32;

#if ARCHSIZE == 64
typedef long int i64;
typedef unsigned long int u64;

typedef long int iptr;
typedef unsigned long int uptr;
#else
typedef long long int i64;
typedef unsigned long long int u64;

typedef int iptr;
typedef unsigned int uptr;
#endif

#ifdef __SIZEOF_INT128__
typedef __int128 i128;
typedef unsigned __int128 u128;
#endif

#ifdef __SIZEOF_FLOAT128__
typedef __float128 f128;
typedef __float128 mfloat;
typedef _Decimal128 d128;
#else
typedef long double mfloat;
#endif

#define __align_mask(x, y) ((__typeof__(x))((y)-1))
#define align_up(x, y) ((((x)-1) | __align_mask(x, y)) + 1)
#define align_down(x, y) ((x) & ~(__align_mask(x, y)))

#define round_down(x, y) (((x) / (y)) * (y))
#define round_up(x, y) (((x + (y - 1)) / (y)) * (y))

#define __min__(x, y) (\
{\
    __typeof__(x) __x = (x); \
    __typeof__(x) __y = (y); \
    __x < __y ? __x : __y; \
})

#define __max__(x, y) (\
{\
    __typeof__(x) __x = (x); \
    __typeof__(x) __y = (y); \
    __x > __y ? __x : __y; \
})

#endif /* TYPES_H_ */

