/*
 * usrlocal.h
 *
 * Created 21 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef USRLOCAL_H_
#define USRLOCAL_H_

int localdata_init ();
char *localdata_path (const char *relpath);

#endif /* USRLOCAL_H_ */

