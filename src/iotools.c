/*
 * ftools.c
 *
 * Created 20 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>

static const FILE *real_stdout = NULL;
static const FILE *real_stderr = NULL;

int stdout_redirect (const char *pathname)
{
    FILE *fout = fopen (pathname, "a");

    if (fout == NULL) {
        perror ("stdout_redirect:fopen()");
        return -1;
    }

    if (real_stdout == NULL)
        real_stdout = fout;
    stdout = fout;

    return 0;
}

int stderr_redirect (const char *pathname)
{
    FILE *ferr = fopen (pathname, "a");

    if (ferr == NULL) {
        perror ("stderr_redirect:fopen()");
        return -1;
    }

    if (real_stderr == NULL)
        real_stderr = ferr;
    stderr = ferr;

    return 0;
}

