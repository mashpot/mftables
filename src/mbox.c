/*
 * mbox.c
 *
 * Created 21 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mbox.h>

#define islwsp(c) (((c) == ' ' || (c) == '\t'))

int mbox_parse (char *buf, size_t len, int state, struct mbox *mb)
{
    char *p, *e, *sp;
    size_t i;

    p = buf + mb->pos;
    sp = p;
    e = p + len;

    switch (state)
    {
      case 0:
        if (len < 5)
            goto end;

        if (*p++ != 'F' ||
            *p++ != 'r' ||
            *p++ != 'o' ||
            *p++ != 'm' ||
            *p++ != ' ')
        {
            return -1;
        }

        ++state;

      case 1:
        sp = p;
        p = strchr (p, ' ');

        if (p == NULL)
            goto end;

        len = p - sp;

        mb->from = malloc (len + 1);

        if (mb->from == NULL) {
            perror ("mbox_parse:malloc()");
            return -1;
        }

        *p++ = 0;
        strcpy (mb->from, sp);

        ++state;

      case 2:
        /* first line datetime */

        sp = p;
        p = strchr (p, '\n');

        if (p == NULL)
            goto end;

        /* Save datetime later */

        *p++ = 0;
        sp = p;

        ++state;

      case 3:
        /* Headers */
        struct mbheader *head;
        char *vp;

        for (; *p != '\n'; sp = p)
        {
            do {
                p = strchr (p, '\n');

                if (p == NULL || p[1] == 0)
                    goto end;
            } while (islwsp (p[1]) && ++p);

            *p++ = 0;
            len = p - sp;
            head = malloc (sizeof (struct mbheader) + len);

            if (head == NULL) {
                perror ("mbox_parse:malloc()");
                return -1;
            }

            vp = (char*)(head + 1);

            head->key = strcpy (vp, sp);
            sp = strchr (vp, ':');

            if (*sp && !memchr (vp, ' ', sp - vp))
                *sp++ = 0;
            head->val = sp;

            if (!(i = mb->headc))
            {
                mb->headc = 1;
                mb->headv = malloc (sizeof (struct mbheader*) * mb->headc);
                if (mb->headv == NULL) {
                    perror ("mbox_parse:malloc()");
                    return -1;
                }
            }
            else
            {
                ++mb->headc;
                mb->headv = realloc (mb->headv, sizeof (struct mbheader*) * mb->headc);
            }
            mb->headv[i] = head;
        }
        sp = p;
        ++state;
        break;

      case 4:

        if (mb->from)
            free (mb->from);
        if (mb->headv) {
            len = mb->headc;
            for (i = 0; i < len; ++i)
                free (mb->headv[i]);
            free (mb->headv);
        }
        memset (mb, 0, sizeof (struct mbox));

        return 0;

      default:
        return -1;
    }

end:
    mb->pos = sp - buf;
    return state;
}

int mbox_print (const struct mbox *mb)
{
    size_t i, len;

    printf ("[mbox] from: %s\n", mb->from);

    len = mb->headc;

    for (i = 0; i < len; ++i)
    {
        printf ("\nkey: %s\nval: %s\n", mb->headv[i]->key, mb->headv[i]->val);
    }

    return 0;
}

