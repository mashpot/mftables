/*
 * addrbook.c
 *
 * Created 30 May 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include <addrbook.h>

struct abhead {
    u32 ndomain;
    u32 nuser;
};

_Static_assert (_Alignof (struct abhead) >= sizeof (u32));

#define OCREAT_MODE (S_IRUSR | S_IWUSR | S_IRGRP)

struct addrbook abook_open(const char *pathname)
{
    struct addrbook ab;
    size_t nbytes, len;
    ssize_t n;
    off_t off;

    memset (&ab, 0, sizeof(ab));

    ab.fd = open (pathname, O_RDWR | O_CREAT, OCREAT_MODE);

    if (ab.fd == -1) {
        perror ("abook_open:open()");
        return ab;
    }

    ab.flen = lseek (ab.fd, 0, SEEK_END);

    if (ab.flen == -1) {
        perror ("abook_open:lseek()");
        goto err1;
    }

    if (ab.flen >= sizeof (struct abhead))
    {
        len = sizeof (struct abhead);
        for (nbytes = 0; nbytes < len; nbytes += n)
        {
            n = pread (ab.fd, ((void*)(&ab)) + nbytes, len - nbytes, nbytes);
            if (n == -1) {
                perror ("abook_open:pread()");
                goto err1;
            }
            if (n == 0) {
                fprintf (stderr, "abook_open: EOF while reading head\n");
                goto err1;
            }
        }
    }

    ab.dlen = align_up ((ab.ndomain+1) * sizeof (struct domain), 4096);
    ab.ulen = align_up ((ab.nuser+1) * sizeof (struct user), 4096);

    ab.dmem = malloc (ab.dlen);
    if (ab.dmem == NULL) {
        perror ("abook_open:malloc()");
        goto err1;
    }

    off = sizeof (struct abhead);
    len = ab.ndomain * sizeof (struct domain);
    for (nbytes = 0; nbytes < len; nbytes += n)
    {
        n = pread (ab.fd, ((void*) ab.dmem) + nbytes, len - nbytes, off + nbytes);
        if (n == -1) {
            perror ("abook_open:pread()");
            goto err2;
        }
        if (n == 0) {
            fprintf (stderr, "abook_open: EOF while reading domains\n");
            goto err2;
        }
    }
    off += len;
    ab.doff = len;

    ab.umem = malloc (ab.ulen);
    if (ab.umem == NULL) {
        perror ("abook_open:malloc()");
        goto err2;
    }

    len = ab.nuser * sizeof (struct user);
    for (nbytes = 0; nbytes < len; nbytes += n)
    {
        n = pread (ab.fd, ((void*) ab.umem) + nbytes, len - nbytes, off + nbytes);
        if (n == -1) {
            perror ("abook_open:pread()");
            goto err3;
        }
        if (n == 0) {
            fprintf (stderr, "abook_open: EOF while reading users\n");
            goto err3;
        }
    }
    ab.uoff += len;

    return ab;

err3:
    free (ab.umem);
err2:
    free (ab.dmem);
err1:
    close (ab.fd);
    ab.fd = -1;
    return ab;
}

static int abwrite(struct addrbook *ab)
{
    size_t len;
    ssize_t n;
    off_t off;
    void *mem;

    off = 0;
    len = sizeof (struct abhead);
    mem = ab;
    for (; off < len; off += n)
    {
        n = pwrite (ab->fd, mem + off, len - off, off);
        if (n == -1) {
            perror ("abwrite:pwrite()");
            return -1;
        }
    }

    n = 0;
    len = ab->ndomain * sizeof (struct domain);

    if (ab->doff < len) {
        mem = ab->dmem;
        mem -= off;
        n = off;

        if (ab->doff)
            off += ab->doff;
        ab->doff = len;
        len += n;

        for (; off < len; off += n)
        {
            n = pwrite (ab->fd, mem + off, len - off, off);
            if (n == -1) {
                perror ("abwrite:pwrite()");
                return -1;
            }
        }
        n = 1;
    } else {
        off += len;
    }

    len = ab->nuser * sizeof (struct user);

    if (n == 1 || ab->uoff < len) {
        mem = ab->umem;
        mem -= off;

        if (n != 1) {
            n = off;

            if (ab->uoff)
                off += ab->uoff;
            ab->uoff = len;
            len += n;
        } else {
            len += off;
        }

        for (; off < len; off += n)
        {
            n = pwrite (ab->fd, mem + off, len - off, off);
            if (n == -1) {
                perror ("abwrite:pwrite()");
                return -1;
            }
        }
    } else {
        off += len;
    }

    ab->flen = off;

    return 0;
}

int abook_close(struct addrbook *ab)
{
    int ret = 0;

    ret = abwrite (ab);
    close (ab->fd);
    free (ab->dmem);
    free (ab->umem);

    return ret;
}

static char *strnchr(const char *s, size_t n, int c)
{
    const char *e = s + n;

    while (s < e && *s != c && *s)
        s++;

    return s < e && *s == c ? ((char*) s) : NULL;
}

int abook_add(struct addrbook *restrict ab, const char *restrict emailaddr)
{
    u32 i;
    struct domain *d;
    struct user *u;
    char *p, *eaddr;

    eaddr = malloc (strlen (emailaddr) + 1);
    if (!eaddr) {
        perror ("abook_add:malloc()");
        return -1;
    }
    strcpy (eaddr, emailaddr);
    p = strnchr (eaddr, AB_MAXUSER+1, '@');

    if (p == NULL) {
        fprintf (stderr, "abook_add: Invalid email address '%s'\n", emailaddr);
        return -1;
    }
    *p++ = 0;

    if (strlen (p) > AB_MAXDOMAIN) {
        fprintf (stderr, "abook_add: Invalid email address\n");
        return -1;
    }

    d = ab->dmem;
    for (i = 0; i < ab->ndomain && strcmp(p, &d[i].name[0]) != 0; ++i);

    if (i == ab->ndomain)
    {
        if ((i+1) * sizeof (struct domain) >= ab->dlen) {
            ab->dlen += 4096;
            ab->dmem = d = realloc (d, ab->dlen);
            if (d == NULL) {
                perror ("abook_add:realloc()");
                return -1;  /* no free of close addrbook */
            }
        }

        d += i;
        memset (d, 0, __builtin_offsetof (struct domain, name[0]));
        strcpy (&d->name[0], p);

        u = ab->umem + ab->nuser;
    }
    else
    {
        u = ab->umem;
        struct user *ue = u + ab->nuser;

        while (u < ue && (u->dom_id != i || strcmp (eaddr, &u->name[0]) != 0)) {
            ++u;
        }

        if (u < ue) {
            //printf ("Email exists\n");
            //return u->score;
            return 0;
        }
    }

    if (ab->ulen <= (size_t)(u + 1) - (size_t)(ab->umem)) {
        ab->ulen += 4096;
        ab->umem = u = realloc (ab->umem, ab->ulen);
        if (u == NULL) {
            perror ("abook_add:realloc()");
            return -1;  /* no free of close addrbook */
        }
        u += ab->nuser;
    }

    if (i == ab->ndomain)
        ++ab->ndomain;
    u->dom_id = i;
    u->score = 0;
    strcpy (&u->name[0], eaddr);
    ab->nuser++;

    return 0;
}

void abook_print(const struct addrbook *ab)
{
    struct user *u, *e;
    struct domain *d = ab->dmem;
    u32 dimax = ab->ndomain;

    u = ab->umem;
    e = u + ab->nuser;
    for (; u < e; ++u)
    {
        if (u->dom_id >= dimax)
            printf ("[ERROR] Corrupt domain index: %s\n", &u->name[0]);
        else
#if 0
          if (u->score >= 255)
#endif
            printf ("[%c] %s@%s\n", u->score > 127 ? '+' : '-', &u->name[0], &d[u->dom_id].name[0]);
    }
}

