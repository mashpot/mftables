/*
 * usrlocal.c
 *
 * Created 21 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>

#define LDIR_DATA_BASE ".local/share"
#define LDIR_CONFIG_BASE ".config"
#define LDIR_MFTABLES_BASE "mftables"

struct localdirs {
    char *home;
    char *data;
    char *config;
};

static struct localdirs localdirs = { 0 };

static char *mftables_data = NULL;
static size_t mftables_len = 0;

#define CHKP_ISBLK      1
#define CHKP_ISCHR      2
#define CHKP_ISIFO      3
#define CHKP_ISREG      4
#define CHKP_ISDIR      5
#define CHKP_ISLNK      6
#define CHKP_ISSOCK     7
#define CHKP_TYPEMASK   7

#define CHKP_F_OK       (F_OK << 3)
#define CHKP_X_OK       (X_OK << 3)
#define CHKP_W_OK       (W_OK << 3)
#define CHKP_R_OK       (R_OK << 3)
#define CHKP_ACSMASK    ((F_OK | X_OK | W_OK | R_OK) << 3)

#define M_OTH (S_IXOTH | S_IWOTH | S_IROTH)
#define M_GRP (S_IXGRP | S_IWGRP | S_IRGRP)
#define M_USR (S_IXUSR | S_IWUSR | S_IRUSR)

#define CHKP_OTH    (M_OTH << 6)
#define CHKP_GRP    (M_GRP << 6)
#define CHKP_USR    (M_USR << 6)
#define CHKP_MODEMASK (CHKP_OTH | CHKP_GRP | CHKP_USR)

static const mode_t modetbl[8] = {
    0,
    __S_IFBLK,
    __S_IFCHR,
    __S_IFIFO,
    __S_IFREG,
    __S_IFDIR,
    __S_IFLNK,
    __S_IFSOCK
};

static int checkpath (const char *pathname, int flags)
{
    struct stat st;
    int n;

    memset (&st, 0, sizeof (st));

    if (stat (pathname, &st) == -1)
        return -1;
    if ((n = (flags & CHKP_TYPEMASK)) && !__S_ISTYPE (st.st_mode, modetbl[n]))
        return 1;
    if ((n = (flags & (CHKP_ACSMASK | CHKP_MODEMASK))))
    {
        /* F_OK is defined as zero, which shouldn't make it passed the
         * sorounding if statement.
         */
        uid_t uid = getuid ();
        gid_t gid = getgid ();
        mode_t mode = (flags & CHKP_MODEMASK) >> 6;

        if ((n & CHKP_X_OK))
            mode |= S_IXUSR | S_IXGRP | S_IXOTH;
        if ((n & CHKP_W_OK))
            mode |= S_IWUSR | S_IWGRP | S_IWOTH;
        if ((n & CHKP_R_OK))
            mode |= S_IRUSR | S_IRGRP | S_IROTH;

        mode = mode & st.st_mode;

        if (st.st_uid != uid) {
            mode &= ~M_USR;
        }
        /* Can't be bothered now but need to getgrouplist(3) <grp.h>
         * because only testing user gid
         */
        if (st.st_gid != gid)
            mode &= ~M_GRP;

        return mode ? 0 : flags & CHKP_MODEMASK;
    }
    return 0;
}

/*
 * Ensures path is a directory, if directory doesn't exist it will be created.
 *
 * Returns zero on success. If path is a non-directory file or an error occurs
 * -1 is returned.
 */
static int ensuredir (const char *path)
{
    struct stat st;

    memset (&st, 0, sizeof (st));

    if (stat (&path[0], &st) != 0)
    {
#if 0
        fprintf (stderr, "dry: mkdir %s\n", &path[0]);
        return -1;
#else
        if (mkdir (&path[0], S_IRWXU) != 0) {
            perror ("localdir");
            return -1;
        }
#endif
    }
    else
    {
        if (!S_ISDIR (st.st_mode)) {
            fprintf (stderr, "localdir: None directory found '%s'\n", &path[0]);
            return -1;
        }
        if (access (&path[0], W_OK) != 0) {
            fprintf (stderr, "localdir:access(): %s '%s'\n", strerror (errno), &path[0]);
            return -1;
        }
    }

    return 0;
}

/*
 * This function is the same as ensuredir, except parent directories after
 * depth that don't exist will be created; path MUST be an absolute path.
 *
 * The root directory '/' is counted as depth zero.
 *
 * On success returns zero, otherwise the depth ensuredir fails is returned.
 */
static int ensuredirfrom (const char *path, int depth)
{
    size_t len = strlen (path);
    char buf[len+1], *p = &buf[0];
    int ndeep = 0;

    for (path++; *path; ++path)
    {
        *p++ = '/';
        while (*path && *path != '/')
            *p++ = *path++;
        *p = 0;
        ++ndeep;
        //printf ("[%i] %s\n", ndeep, &buf[0]);
        if (ndeep > depth && ensuredir (&buf[0]) != 0)
            return ndeep;
    }

    return 0;
}

int localdata_init ()
{
    const char *home_path;
    char *p;
    size_t len;

    if (localdirs.home)
        return 0;
    memset (&localdirs, 0, sizeof (struct localdirs));

#if _GNU_SOURCE
    home_path = secure_getenv ("HOME");
#else
    home_path = getenv ("HOME");
#endif

    if (!home_path)
    {
        struct passwd *pw;
        /* If one wants to check errno after the call, it should be set to zero
         * before the call. See getpwuid(3) */
        errno = 0;
        pw = getpwuid (getuid ());

        if (!pw) {
            perror ("localdir:getpwuid()");
            return -1;
        }

        home_path = pw->pw_dir;
    }

    if (checkpath (home_path, CHKP_ISDIR)) {
        fprintf (stderr, "mftables: [ERROR] Bad home directory permissions.\n");
        return -1;
    }

    /*
     * /home/USER
     */
    len = strlen (home_path)+1;
    if (home_path[-1] != '/')
        ++len;
    localdirs.home = malloc (len);

    if (localdirs.home == NULL) {
        perror ("localdata_init");
        return -1;
    }
    p = stpcpy (localdirs.home, home_path);

    if (p[-1] != '/') {
        *p++ = '/';
        *p = 0;
    }

    /*
     * /home/USER/.local/share
     */
    localdirs.data = malloc (len + sizeof (LDIR_DATA_BASE));

    if (localdirs.data == NULL) {
        perror ("localdata_init");
        goto err;
    }
    p = stpcpy (localdirs.data, localdirs.home);
    strcpy (p, LDIR_DATA_BASE);

    /*
     * /home/USER/.config
     */
    localdirs.config = malloc (len + sizeof (LDIR_CONFIG_BASE));

    if (localdirs.config == NULL) {
        perror ("localconfig_init");
        goto err;
    }
    p = stpcpy (localdirs.config, localdirs.home);
    strcpy (p, LDIR_CONFIG_BASE);

    /*
     * /home/USER/.local/share/mftables
     */
    len = len + sizeof (LDIR_CONFIG_BASE)+1 + sizeof (LDIR_MFTABLES_BASE);
    mftables_data = malloc (len);

    if (mftables_data == NULL) {
        perror ("localconfig_init");
        goto err;
    }
    p = stpcpy (mftables_data, localdirs.data);
    *p++ = '/';
    strcpy (p, LDIR_MFTABLES_BASE);

    if (ensuredirfrom (mftables_data, 2))
        goto err;
    mftables_len = len;

    return 0;

err:
    if (localdirs.home)
        free (localdirs.home);
    if (localdirs.data)
        free (localdirs.data);
    if (localdirs.config)
        free (localdirs.config);
    if (mftables_data)
        free (mftables_data);
    mftables_data = NULL;
    memset (&localdirs, 0, sizeof (struct localdirs));

    return -1;
}

char *localdata_path (const char *relpath)
{
    size_t len = strlen (relpath) + mftables_len + 2;
    char *p, *path = malloc (len);

    if (!path) {
        perror ("localdata_path");
        return NULL;
    }
    p = stpcpy (path, mftables_data);
    *p++ = '/';
    strcpy (p, relpath);

    return path;
}

