#
# Makefile
#
# Created 30 May 2023 by Mason Hall (mason@mashpot.net)
#
# Copyright (C) 2023 Mason Hall
#

OUTPUT	    := mftables
CC	        := gcc
CFLAGS	    := -fdiagnostics-color=always -Wall -O3 -g 
# -fsanitize=address
INCLUDES	:= -I"./src/include" -include"./src/include/types.h"
LIBS	    := -lc -lm

FILES = main.c addrbook.c parse.c iotools.c filter.c localdata.c mbox.c

SRCS := $(addprefix ./src/, $(FILES))
OBJS := $(addprefix ./build/src/, $(FILES:.c=.o))
YEAR := $(shell date +%Y)

default: all

all: $(OUTPUT)

$(OUTPUT) : $(OBJS)
	@echo "$@"
	$(CC) $(CFLAGS) $(OBJS) $(LIBS) -o $@

build/src/%.o : ./src/%.c
	mkdir -p $(@D)
	@echo "Building $<"
	$(CC) $(CFLAGS) $(INCLUDES) -D__YEAR__=$(YEAR) -MD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c "$<" -o "$@"
	@echo " "

clean:
	-rm -f $(OUTPUT) $(OBJS)

